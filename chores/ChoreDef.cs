﻿namespace chores
{
    public class ChoreDef
    {
        public string Category { get; set; }
        public string Key { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
