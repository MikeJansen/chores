﻿namespace chores
{
    public enum ScheduleType { Daily, BiWeekly, Weekly }

    public class Schedule
    {
        public ChoreDef Chore { get; set; }
        public ScheduleType Type { get; set; }
        public int Index { get; set; }
        public Person Person { get; set; }

        public int Order
        {
            get
            {
                switch(Type)
                {
                    case ScheduleType.Daily:
                        return Index * 10;
                    case ScheduleType.BiWeekly:
                        switch (Index)
                        {
                            case 1:
                                return 5;
                            case 2:
                                return 35;
                            case 3:
                                return 75;
                            default:
                                return 105;
                        }
                    default:
                        switch (Index)
                        {
                            case 1:
                                return 1;
                            default:
                                return 71;
                        }
                }
            }
        }
    }
}
