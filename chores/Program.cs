﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace chores
{
    class Program
    {
        static Dictionary<string, Person> people = new Dictionary<string, Person>();
        static Dictionary<string, ChoreDef> chores = new Dictionary<string,ChoreDef>();
        static List<Schedule> schedules = new List<Schedule>();

        static Person GetPerson(string key)
        {
            Person person;
            if (!people.TryGetValue(key, out person))
            {
                person = new Person { Key = key, Name = key };
                people[key] = person;
            }
            return person;
        }

        static void Main(string[] args)
        {
            DateTime startDate = DateTime.Parse(args[0]);
            Assembly assembly = Assembly.GetExecutingAssembly();

            using (Stream choreStream = assembly.GetManifestResourceStream("chores.chore-defs.txt"))
            using (TextReader choreReader = new StreamReader(choreStream))
            {
                string line = choreReader.ReadLine();
                while ((line = choreReader.ReadLine()) != null)
                {
                    string[] choreParts = line.Split('\t');
                    ChoreDef choreDef = new ChoreDef
                    {
                        Category = choreParts[0],
                        Key = choreParts[1],
                        Name = choreParts[2],
                        Description = choreParts[3]
                    };
                    chores[choreDef.Key] = choreDef;
                }
            }

            using (Stream schedStream = assembly.GetManifestResourceStream("chores.schedule.txt"))
            using (TextReader schedReader = new StreamReader(schedStream))
            {
                string line = schedReader.ReadLine();
                while ((line = schedReader.ReadLine()) != null)
                {
                    string[] schedParts = line.Split('\t');
                    Schedule sched = new Schedule
                    {
                        Chore = chores[schedParts[0]],
                        Type = GetSchedType(schedParts[1]),
                        Index = int.Parse(schedParts[2]),
                        Person = GetPerson(schedParts[3])
                    };
                    schedules.Add(sched);
                }
            }

            foreach (Person person in (from p in people.Values where p.Key != "ALL" orderby p.Name select p))
            {
                IEnumerable<Schedule> mySchedules = 
                    from s in schedules 
                    where s.Person.Key == person.Key || s.Person.Key == "ALL" 
                    select s;

                OutputHeader(person.Name);
                Output();

                OutputHeader("Week 1");
                OutputChores(from s in mySchedules where s.Type == ScheduleType.Weekly && s.Index == 1 select s.Chore);
                
                OutputHeader("1st Half");
                OutputChores(from s in mySchedules where s.Type == ScheduleType.BiWeekly && s.Index == 1 select s.Chore);

                OutputDateHeader(startDate);
                OutputChores(from s in mySchedules where s.Type == ScheduleType.Daily && s.Index == 1 select s.Chore);

                OutputDateHeader(startDate.AddDays(1.0));
                OutputChores(from s in mySchedules where s.Type == ScheduleType.Daily && s.Index == 2 select s.Chore);

                OutputDateHeader(startDate.AddDays(2.0));
                OutputChores(from s in mySchedules where s.Type == ScheduleType.Daily && s.Index == 3 select s.Chore);

                OutputHeader("2nd Half");
                OutputChores(from s in mySchedules where s.Type == ScheduleType.BiWeekly && s.Index == 2 select s.Chore);

                OutputDateHeader(startDate.AddDays(3.0));
                OutputChores(from s in mySchedules where s.Type == ScheduleType.Daily && s.Index == 4 select s.Chore);

                OutputDateHeader(startDate.AddDays(4.0));
                OutputChores(from s in mySchedules where s.Type == ScheduleType.Daily && s.Index == 5 select s.Chore);

                OutputDateHeader(startDate.AddDays(5.0));
                OutputChores(from s in mySchedules where s.Type == ScheduleType.Daily && s.Index == 6 select s.Chore);

                OutputDateHeader(startDate.AddDays(6.0));
                OutputChores(from s in mySchedules where s.Type == ScheduleType.Daily && s.Index == 7 select s.Chore);

                OutputHeader("Week 2");
                OutputChores(from s in mySchedules where s.Type == ScheduleType.Weekly && s.Index == 2 select s.Chore);

                OutputHeader("1st Half");
                OutputChores(from s in mySchedules where s.Type == ScheduleType.BiWeekly && s.Index == 3 select s.Chore);

                OutputDateHeader(startDate.AddDays(7.0));
                OutputChores(from s in mySchedules where s.Type == ScheduleType.Daily && s.Index == 8 select s.Chore);

                OutputDateHeader(startDate.AddDays(8.0));
                OutputChores(from s in mySchedules where s.Type == ScheduleType.Daily && s.Index == 9 select s.Chore);

                OutputDateHeader(startDate.AddDays(9.0));
                OutputChores(from s in mySchedules where s.Type == ScheduleType.Daily && s.Index == 10 select s.Chore);

                OutputHeader("2nd Half");
                OutputChores(from s in mySchedules where s.Type == ScheduleType.BiWeekly && s.Index == 4 select s.Chore);

                OutputDateHeader(startDate.AddDays(10.0));
                OutputChores(from s in mySchedules where s.Type == ScheduleType.Daily && s.Index == 11 select s.Chore);

                OutputDateHeader(startDate.AddDays(11.0));
                OutputChores(from s in mySchedules where s.Type == ScheduleType.Daily && s.Index == 12 select s.Chore);

                OutputDateHeader(startDate.AddDays(12.0));
                OutputChores(from s in mySchedules where s.Type == ScheduleType.Daily && s.Index == 13 select s.Chore);

                OutputDateHeader(startDate.AddDays(13.0));
                OutputChores(from s in mySchedules where s.Type == ScheduleType.Daily && s.Index == 14 select s.Chore);

                Output("\f");
            }


        }

        private static void OutputDateHeader(DateTime dateTime)
        {
            OutputHeader("{0:ddd MMM d}", dateTime);
        }

        private static void OutputHeader(string format, params object[] args)
        {
            string value = string.Format(format, args);
            StringBuilder builder = new StringBuilder();
            builder.Append("-- ").Append(value).Append(" ");
            for (int index = 0; index < 75 - value.Length; index++)
            {
                builder.Append('-');
            }
            Output(builder.ToString());
        }

        private static void OutputChores(IEnumerable<ChoreDef> choreDefs)
        {
            foreach (ChoreDef choreDef in (from c in choreDefs orderby c.Category, c.Name select c))
            {
                OutputChore(choreDef);
            }
        }

        private static void OutputChore(ChoreDef choreDef)
        {
            Output("    [  ] {0}: {1}", choreDef.Category, choreDef.Name);
        }

        private static ScheduleType GetSchedType(string schedType)
        {
            switch (schedType)
            {
                case "day":
                    return ScheduleType.Daily;
                case "week":
                    return ScheduleType.Weekly;
                default:
                    return ScheduleType.BiWeekly;
            }
        }

        private static void Output()
        {
            Console.WriteLine();
        }

        private static void Output(string format, params object[] args)
        {
            Console.WriteLine(format, args);
        }
    }
}
